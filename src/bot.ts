import {  Client, Snowflake, Message, Collection, Guild } from 'discord.js';
import * as token from '../token';
import { Command } from './command';
import { GuildConfig } from './guildConfig';
//import { HarbingerDatabase } from './database';
//import { Logger } from 'js-logger';
import help from './commands/help';
import join from './zcommands/join';
import swap from './zcommands/swap';
import startgame from './zcommands/startgame';
import endgame from './zcommands/endgame';
import debug from './commands/debug';

class Harbinger
{
    client: Client;
    token: string;
    guilds: Collection<Snowflake, GuildConfig>;
    commands: Collection<string, Command>;
//    db: HarbingerDatabase;
    logger: any;

    constructor()
    {
        this.client = new Client();
        this.token = process.env['TOKEN'] || (<any>token).token;
        this.guilds = new Collection();  
        this.commands = new Collection();
        this.registerCommands();
        //this.db = new HarbingerDatabase('./harbinger.sqlite');
        this.logger = console;
    }

    start()
    {
        this.client.on('ready', () => {
            this.logger.info('Harbinger active.');
            //this.logger.info(`Harbinger logged in with token: ${this.token}`);
            this.client.user.setActivity('HvZ | !join | !switch');
        });

        this.client.on('message', (message) => {
            //Don't respond to bots; Bots are evil.
            if(message.author.bot) return;
            if(message.channel.type === "text")
            {
                this.handleMessage(message);
            }
            if(message.channel.type === "dm")
            {
                this.handleDM(message);
            }
        });

        this.client.on('guildMemberAdd', (member) =>
        {
            let config = this.findConfig(member.guild);
            if(config.getConfig().enableJoinMessage)
            {
                member.createDM().then(channel => 
                {
                    channel.send(`Hey! Welcome to ${member.guild.name}! I'm Harbinger ${this.client.emojis.find("name", "wave")}. To join the human team, type \`!${config.getPrefix()}join\`. If you get tagged, type \`${config.getPrefix()}swap\`. If you have any questions or issues, please contact UberPilot#1658.`)
                })
            }
        })

        this.client.login(this.token);
    }

    handleMessage(message: Message)
    {
        let guild = message.guild;
        let config = this.findConfig(guild);
        //This is not a message for us.
        if(!message.cleanContent.startsWith(config.getPrefix()))
            return;

        let args = message.content.split(' ')
        let command = args[0].substring(1);

        if(this.commands.has(command))
            this.commands.get(command)!.run(message, args, config, this.client);
    }

    handleDM(message: Message)
    {
        //Currently no support for DMs, but create method for handling them for future use.
        return;
    }

    findConfig(guild: Guild): GuildConfig
    {
        if(this.guilds.has(guild.id))
            return this.guilds.get(guild.id)!;
        else
            return this.makeConfig(guild);
    }

    makeConfig(guild: Guild): GuildConfig
    {
        let config = new GuildConfig(guild);
        this.guilds.set(guild.id, config);
        return config;
    }

    registerCommands()
    {
        //Register commands here. Conflicting aliases are logged.
        let commands = [help, join, swap, startgame, endgame, debug];

        for(const command of commands)
        {
            //console.log(`Loading ${command.getName()} with aliases ${command.getAliases()}`)
            for(const alias of command.getAliases())
            {
                if(!this.commands.has(alias))
                    this.commands.set(alias, command);
                else
                    console.error(`Conflicting alias: ${command.getName()}.${alias} and ${this.commands.get(alias)!.getName()}.${alias}`)
            }
        }
    }
}

new Harbinger().start();